<?php

/**
 * @file
 * API documentation for Marketo MA Webform UUID.
 */

/**
 * Change the lead data before send.
 *
 * @param array $data
 *   A structured array suitable for syncLead().
 * @param object $node
 *   A structured node object.
 * @param object $submission
 *   A structured webform submission object.
 */
function hook_marketo_ma_webform_uuid_marketo_data_alter(&$data, $node, $submission) {
}

/**
 * Allow or not allow lead association in $allow_associate_lead variable
 *
 * @param bool $allow_associate_lead
 * @param array $data
 *   A structured array suitable for syncLead().
 * @param object $node
 *   A structured node object.
 * @param object $submission
 *   A structured webform submission object.
 */
function hook_marketo_ma_webform_uuid_associate_lead_allow_alter($allow_associate_lead, $data, $node, $submission) {
}

/**
 * Do something after lead send.
 *
 * @param array $data
 *   A structured array suitable for syncLead().
 */
function hook_marketo_ma_webform_uuid_marketo_send_lead_after_alter(&$data) {
}